import React from 'react';
import { Text, View, StyleSheet, Image, NetInfo, ToastAndroid } from 'react-native';
import { TabNavigator } from 'react-navigation'; // Version can be specified in package.json


class HomeScreen extends React.Component {
    render() {
        return (
            <View style={ styles.viewstyle }>
                <Image source={require('./imgs/jinnyjoe.png')} style={{position: 'absolute' }}/>

            </View>
        );
    }
}

class SettingsScreen extends React.Component {
    render() {
        return (
            <View style={ styles.viewstyle }>
                <Image source={require('./imgs/jinnyjoe.png')} style={{position: 'absolute' }}/>
                <Text>Settings!</Text>
            </View>
        );
    }
}

class InboxScreen extends React.Component {
    render() {
        return (
            <View style={ styles.viewstyle }>
                <Image source={require('./imgs/jinnyjoe.png')} style={{position: 'absolute' }}/>
                <Text>Inbox!</Text>
            </View>
        );
    }
}
class OutboxScreen extends React.Component {
    render() {
        return (
            <View style={ styles.viewstyle }>
    <Image source={require('./imgs/jinnyjoe.png')} style={{position: 'absolute' }}/>
                <Text>Outbox!</Text>
            </View>
        );
    }
}

const TabNav = TabNavigator ({
    Home: {screen: HomeScreen},
    Settings: {screen: SettingsScreen},
    Inbox: {screen: InboxScreen},
    Outbox: {screen: OutboxScreen}},
    {
        tabBarPosition: 'bottom',
        animationEnabled: true,
        initialRouteName: 'Home',
        headerMode: 'none',
        tabBarOptions: {
            inactiveTintColor: '#bbbbbb',
            activeTintColor: '#fff',
            labelStyle: {
                fontSize: 9,
                marginBottom: 5,
                marginTop: 0,
                fontFamily: 'Quicksand-Medium'
            },
            style: {
                backgroundColor: '#4d87ee',
                borderWidth: 0,
                borderTopColor: 'transparent',
                shadowOpacity: 0.6,
                shadowRadius: 4,
                shadowColor: '#212121',
                shadowOffset: {
                    width: 0,
                    height: 0
                },

            }
        }

});

const styles = StyleSheet.create({
    viewstyle: {
        backgroundColor: '#06A0FF',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',


    },
});

export default class Contact extends React.Component {
    render() {
        NetInfo.isConnected.fetch().then(isConnected => {
            ToastAndroid.show('First, is ' + (isConnected ? 'online' : 'offline'),ToastAndroid.SHORT);
        });
        function handleFirstConnectivityChange(isConnected) {
            ToastAndroid.show('Then, is ' + (isConnected ? 'online' : 'offline'),ToastAndroid.SHORT);
            NetInfo.isConnected.removeEventListener(
                'connectionChange',
                handleFirstConnectivityChange
            );
        }
        NetInfo.isConnected.addEventListener(
            'connectionChange',
            handleFirstConnectivityChange
        );


        return (
            <TabNav style={{position: 'absolute', top: 0, left: 0, flex: 1}} />
        );
    }
}
module.export = Contact;
